//
//  SearchViewController.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 06/06/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import UIKit

protocol Search: NSObjectProtocol {
    func search(query: String, method: String)
}

class SearchViewController: UIViewController {
    
    weak var searchDelegate: Search?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addKeyboardHidding()
    }
    
    @IBOutlet var queryTextField: UITextField!
    
    @IBAction func byName(_ sender: UIButton) {
        self.searchDelegate?.search(query: self.queryTextField.text ?? "", method: "name")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func byDate(_ sender: UIButton) {
        self.searchDelegate?.search(query: self.queryTextField.text ?? "", method: "date")
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func byImportancy(_ sender: UIButton) {
        self.searchDelegate?.search(query: self.queryTextField.text ?? "", method: "importancy")
        self.navigationController?.popViewController(animated: true)
    }
}
