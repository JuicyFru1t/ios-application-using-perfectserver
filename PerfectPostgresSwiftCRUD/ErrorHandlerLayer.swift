//
//  ErrorHandlerLayer.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 16/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import Foundation
import UIKit

//Обработка ошибок
class ErrorHandlerLayer {
    class func throwError(title: String) -> UIAlertController {
        let alert = UIAlertController(title: "Ошибка", message: title, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: nil))
        return alert
    }
}
