//
//  PhotoDocument.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 16/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import Foundation

protocol Prototype {
    func copy() -> Prototype
}

class PhotoDocument: Prototype {
    
    func copy() -> Prototype {
        return PhotoDocument(photo: self)
    }
    
    let description: String
    let date: String
    let id: Int
    let imageData: String
    let importancy: Int
    
    //JSON Object
    private var JSON: [String: Any] {
        get {
            return ["description": self.description, "importancy": self.importancy, "date": self.date, "data": self.imageData, "userId": User.userId, "id" : self.id]
        }
    }
    
    //for prototype initialisation
    private init(photo: PhotoDocument) {
        self.description = photo.description
        self.date = photo.date
        self.importancy = photo.importancy
        self.imageData = photo.imageData
        self.id = photo.id
    }
    
    //for create initialisation
    init(description: String, imageData: String, importancy: Int) {
        self.description = description
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        self.date = dateFormatter.string(from:Date())
        self.importancy = importancy
        self.imageData = imageData
        self.id = 0
    }
    
    //from server initialisation
    init(description: String, imageData: String, importancy: Int, date: String, id: Int) {
        self.description = description
        self.date = date
        self.id = id
        self.importancy = importancy
        self.imageData = imageData
    }
    
    //create self
    func create(handler: @escaping (String) -> ()) {
        APILayer.shared.request(parameters: self.JSON, url: "photo/create", method: .post) { responce in
            if let ok = responce?["succses"].string {
                handler(ok)
            } else {
                if let error = responce?["error"].string {
                    handler (error)
                } else {
                    handler ("Ошибка сети")
                }
            }
            
        }
    }
    
    //delete self
    func delete(handler: @escaping (String) -> ()) {
        APILayer.shared.request(parameters: self.JSON, url: "photo/delete", method: .post) { responce in
            if let ok = responce?["succses"].string {
                handler(ok)
            } else {
                if let error = responce?["error"].string {
                    handler (error)
                } else {
                    handler ("Ошибка сети")
                }
            }
            
        }
    }
}
