//
//  APILayer.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 16/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

//Доступ к серверу
class APILayer {
    static let shared = APILayer()
    private init() {}
    
    private let serverAddress = "http://192.168.0.22:8080/"
    
    func request(parameters: [String: Any], url: String, method: HTTPMethod, handler: @escaping (JSON?) -> Void) {
        Alamofire.request(self.serverAddress + url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON { response in
            if let data = response.result.value {
                handler(JSON(data))
            }
        }
    }
}
