//
//  ImageTableViewCell.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 16/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import UIKit

class ImageTableViewCell: UITableViewCell {
    @IBOutlet var name: UILabel!
    @IBOutlet var imagee: UIImageView!
}
