//
//  AppendingViewController.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 16/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import UIKit

protocol Append: NSObjectProtocol {
    func didAppend()
}

class AppendingViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //Окно выбора фото
    let imagePicker = UIImagePickerController()
    weak var appendDelegate: Append?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.allowsEditing = true
        self.addKeyboardHidding()
    }

    @IBOutlet var importancyTextField: UITextField!
    @IBOutlet var descriptionTextField: UITextField!
    @IBOutlet var appendingImage: UIImageView!
    
    @IBAction func choosePhoto(_ sender: UIButton) {
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    //Делегированная фукнция выбора фото
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage {
            self.appendingImage.image = pickedImage
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveData(_ sender: UIButton) {
        if let image = self.appendingImage.image {
            let imageb: Data = UIImageJPEGRepresentation(image, 0.7)!
            let dataDecoded: String = imageb.base64EncodedString(options: .lineLength64Characters)
            
            let description = self.descriptionTextField.text!
            let importancy = Int(self.importancyTextField.text!)!
            
            let photoDocument = PhotoDocument( description: description, imageData: dataDecoded, importancy: importancy)
            photoDocument.create() { [weak self] result in
                guard let `self` = self else {
                    return
                }
                if result == "Ok" {
                    self.appendDelegate?.didAppend()
                    self.navigationController?.popViewController(animated: true)
                } else {
                    self.throwError(title: result)
                }
            }
        } else {
            self.throwError(title: "Выберите фото")
        }
    }
}
