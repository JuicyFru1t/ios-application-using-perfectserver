//
//  User.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 16/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import Foundation
import SwiftyJSON

class User {
    
    let name: String
    let password: String
    
    init(name: String, password: String) {
        self.name = name
        self.password = password
    }
    
    private var JSON: [String: Any] {
        get {
            return ["name": self.name, "password": self.password]
        }
    }
    
    //user id auth
    static var userId: Int {
        get {
            return defaults.integer(forKey: "userId")
        }
        
        set {
            defaults.set(newValue, forKey: "userId")
            defaults.synchronize()
        }
    }
    
    //auth / search
    func auth(handler: @escaping (String) -> ()) {
        APILayer.shared.request(parameters: self.JSON, url: "user/auth", method: .post) { responce in
            if let userId = responce?["userId"].int {
                User.userId = userId
                handler("Ok")
            } else {
                if let error = responce?["error"].string {
                    handler (error)
                } else {
                    handler ("Ошибка сети")
                }
            }
        }
    }
    
    //register / create
    func login(handler: @escaping (String) -> ()) {
        APILayer.shared.request(parameters: self.JSON, url: "user/create", method: .post) { responce in
            if let userId = responce?["userId"].int {
                User.userId = userId
                handler("Ok")
            } else {
                if let error = responce?["error"].string {
                    handler (error)
                } else {
                    handler ("Ошибка сети")
                }
            }
        }
    }
    
    //find photos
    class func getPhotos(handler: @escaping ([PhotoDocument]) -> ()) {
        APILayer.shared.request(parameters: ["userId": User.userId], url: "user/getPhotos", method: .post) { responce in
            var photos: [PhotoDocument] = []
            if let photosData = responce?["photos"].array {
                for photo in photosData {
                    let id = photo["id"].intValue
                    let description = photo["description"].stringValue
                    let date = photo["date"].stringValue
                    let imageData = photo["data"].stringValue
                    let importancy = photo["importancy"].intValue
                    let photo = PhotoDocument(description: description, imageData: imageData, importancy: importancy, date: date, id: id)
                    photos.append(photo)
                }
            }
            handler(photos)
        }
    }
    
    class func getPhotosWithParameters(query: String, sortMethod: String, handler: @escaping ([PhotoDocument]) -> ()) {
        APILayer.shared.request(parameters: ["userId": User.userId, "query": query, "sortMethod": sortMethod], url: "user/getPhotosWithParameters", method: .post) { responce in
            var photos: [PhotoDocument] = []
            if let photosData = responce?["photos"].array {
                for photo in photosData {
                    let id = photo["id"].intValue
                    let description = photo["description"].stringValue
                    let date = photo["date"].stringValue
                    let imageData = photo["data"].stringValue
                    let importancy = photo["importancy"].intValue
                    let photo = PhotoDocument(description: description, imageData: imageData, importancy: importancy, date: date, id: id)
                    photos.append(photo)
                }
            }
            handler(photos)
        }
    }
}
