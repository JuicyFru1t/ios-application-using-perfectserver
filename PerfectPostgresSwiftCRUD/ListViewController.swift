//
//  ListViewController.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 16/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, Search, Append {
    
    func search(query: String, method: String) {
        User.getPhotosWithParameters(query: query, sortMethod: method) { [weak self] result in
            guard let `self` = self else {
                return
            }
            
            self.photos = result
            self.imagesTableView.reloadData()
        }
    }
    
    func didAppend() {
        User.getPhotos { [weak self] result in
            guard let `self` = self else {
                return
            }
            
            self.photos = result
            self.imagesTableView.reloadData()
        }
    }
    
    private var photos: [PhotoDocument] = []
    @IBOutlet var imagesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        User.getPhotos { [weak self] result in
            guard let `self` = self else {
                return
            }
            
            self.photos = result
            self.imagesTableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.photos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as! ImageTableViewCell
        let data = Data(base64Encoded: self.photos[indexPath.row].imageData, options: .ignoreUnknownCharacters)
        cell.imagee.image = UIImage(data: data!)
        cell.name.text = self.photos[indexPath.row].description
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ImageTableViewCell
        ExportLayer.export(photo: cell.imagee.image!, target: self)
        let ac = UIAlertController(title: "Успешно", message: "Фото сохранено в библиотеке", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let action = UITableViewRowAction(style: .default, title: "Удалить", handler: { [weak self] (action, indexPath) in
            
            guard let `self` = self else {
                return
            }

            self.photos[indexPath.row].delete() { [weak self] result in
                guard let `self` = self else {
                    return
                }
                if result == "Ok" {
                    self.photos.remove(at: indexPath.row)
                    self.imagesTableView.reloadData()
                } else {
                    self.throwError(title: result)
                }
            }
        })
        
        return [action]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? SearchViewController {
            destination.searchDelegate = self
        } else if let destination = segue.destination as? AppendingViewController {
            destination.appendDelegate = self
        }
    }
}
