//
//  AppDelegate.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 15/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import UIKit
import Alamofire

var defaults = UserDefaults.standard
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}

