//
//  ExportLayer.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 17/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import Foundation
import UIKit

//Слой, отвечающий 
class ExportLayer {
    class func export(photo: UIImage, target: UIViewController) {
        UIImageWriteToSavedPhotosAlbum(photo, target, nil, nil)
    }
}
