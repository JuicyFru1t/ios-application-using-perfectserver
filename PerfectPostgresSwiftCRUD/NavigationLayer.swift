//
//  NavigationLayer.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 16/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import Foundation
import UIKit

//Слой, отвечающий за навигацию
class NavigationLayer {
    class func authSuccsseded() -> UINavigationController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let targetVC = storyboard.instantiateViewController(withIdentifier: "library") as! UINavigationController
        return targetVC
    }
}
