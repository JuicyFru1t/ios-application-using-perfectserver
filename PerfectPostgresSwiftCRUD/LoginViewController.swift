//
//  LoginViewController.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 16/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addKeyboardHidding()
    }
    
    @IBOutlet var loginTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    //Регистрация
    @IBAction func register(_ sender: UIButton) {
        if self.validate() {
            let user = User(name: loginTextField.text!, password: passwordTextField.text!)
            user.login { [weak self] result in
                
                guard let `self` = self else {
                    return
                }
                
                if result == "Ok" {
                    self.authSuccsseded() 
                } else {
                    self.throwError(title: result)
                }
            }
        }
    }
    
    //Авторизация
    @IBAction func auth(_ sender: UIButton) {
        if self.validate() {
            let user = User(name: loginTextField.text!, password: passwordTextField.text!)
            user.auth { [weak self] result in
                guard let `self` = self else {
                    return
                }
                
                if result == "Ok" {
                    self.authSuccsseded()
                } else {
                    self.throwError(title: result)
                }
            }
        }
    }
    
    //Валидация полей
    private func validate() -> Bool {
        if let login = loginTextField.text, let password = passwordTextField.text {
            if login != "" && password != "" {
                return true
            } else {
                self.throwError(title: "Пустые поля")
                return false
            }
        } else {
            self.throwError(title: "Пустые поля")
            return false
        }
    }
    
    private func authSuccsseded() {
        let targetVC = NavigationLayer.authSuccsseded()
        self.present(targetVC, animated: true, completion: nil)
    }
}
