//
//  Extensions.swift
//  PerfectPostgresSwiftCRUD
//
//  Created by Alexander Khodko on 16/05/2017.
//  Copyright © 2017 Alexander Khodko. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func throwError(title: String) {
        let alert = ErrorHandlerLayer.throwError(title: title)
        self.present(alert, animated: true, completion: nil)
    }
    
    func addKeyboardHidding() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(gesture)
    }
    
    @objc private func hideKeyboard() {
        self.view.endEditing(true)
    }
}
